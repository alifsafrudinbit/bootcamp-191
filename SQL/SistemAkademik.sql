1.
SELECT "NM_FAKULTAS","NM_JURUSAN" FROM "Fakultas","Jurusan";


2.
SELECT T1."NM_FAKULTAS", T2."NM_JURUSAN", T2."KD_JURUSAN"
FROM "Fakultas" T1 INNER JOIN "Jurusan" T2 ON T1."KD_FAKULTAS" = T2."KD_FAKULTAS"
WHERE "NM_FAKULTAS" LIKE '%fi%';


3.
SELECT T1."NM_FAKULTAS", T2."NM_JURUSAN", T2."KD_JURUSAN"
FROM "Fakultas" T1 INNER JOIN "Jurusan" T2 ON T1."KD_FAKULTAS" = T2."KD_FAKULTAS"
WHERE "NM_JURUSAN" LIKE '%ta%';			


4.
SELECT A1."NM_FAKULTAS", COUNT(A2."KD_FAKULTAS") AS "JML_JURUSAN"
FROM "Fakultas" A1 LEFT JOIN "Jurusan" A2 ON A1."KD_FAKULTAS" = A2."KD_FAKULTAS"
GROUP BY A1."NM_FAKULTAS";


5.
SELECT A1."NM_FAKULTAS", COUNT(A2."KD_FAKULTAS") AS "JML_JURUSAN"
FROM "Fakultas" A1 LEFT JOIN "Jurusan" A2 ON A1."KD_FAKULTAS" = A2."KD_FAKULTAS"
GROUP BY A1."NM_FAKULTAS"
HAVING COUNT (A2."NM_JURUSAN")>2;


6.
SELECT A1."NM_FAKULTAS", COUNT(A2."KD_FAKULTAS") AS "JML_JURUSAN"
FROM "Fakultas" A1 LEFT JOIN "Jurusan" A2 ON A1."KD_FAKULTAS" = A2."KD_FAKULTAS"
GROUP BY A1."NM_FAKULTAS"
HAVING COUNT (A2."NM_JURUSAN")=0;


7.
SELECT N1."NM_MK", N2."NM_JURUSAN", N3."NM_FAKULTAS"
FROM "Matakuliah" N1 LEFT JOIN "Jurusan" N2 ON N1."KD_JURUSAN" = N2."KD_JURUSAN" LEFT JOIN "Fakultas" N3 ON N2."KD_FAKULTAS" = N3."KD_FAKULTAS"
ORDER BY "NM_FAKULTAS"; 


8.
SELECT jrs."NM_JURUSAN", COUNT (mk."KD_MK") AS "JML_MK"
FROM "Jurusan" jrs LEFT JOIN "Matakuliah" mk ON jrs."KD_JURUSAN" = mk."KD_JURUSAN"
GROUP BY jrs."NM_JURUSAN";


9.
SELECT jrs."NM_JURUSAN", COUNT (mk."KD_MK") AS "JML_MK", mk."SKS"
FROM "Jurusan" jrs LEFT JOIN "Matakuliah" mk ON jrs."KD_JURUSAN" = mk."KD_JURUSAN"
WHERE mk."SKS" = 2
GROUP BY jrs."NM_JURUSAN", mk."SKS";


10.
SELECT jrs."NM_JURUSAN", COUNT (mk."KD_MK") AS "JML_MK", mk."SKS"
FROM "Jurusan" jrs LEFT JOIN "Matakuliah" mk ON jrs."KD_JURUSAN" = mk."KD_JURUSAN"
WHERE mk."SKS" = 3
GROUP BY jrs."NM_JURUSAN", mk."SKS";


11.
SELECT "NM_FAKULTAS", "NM_JURUSAN", "NM_MK"
FROM "Fakultas" fk RIGHT JOIN "Jurusan" jrs ON fk."KD_FAKULTAS" = jrs."KD_FAKULTAS" FULL JOIN "Matakuliah" mk ON jrs."KD_JURUSAN" = mk."KD_JURUSAN"
GROUP BY  "NM_FAKULTAS", "NM_JURUSAN", "NM_MK"
HAVING COUNT ("NM_MK") = 0;


12.
SELECT "NM_MK", "NM_JURUSAN", "NM_FAKULTAS"
FROM "Matakuliah" mk LEFT JOIN "Jurusan" jrs ON mk."KD_JURUSAN" = jrs."KD_JURUSAN"
LEFT JOIN "Fakultas" fk ON jrs."KD_FAKULTAS" = fk."KD_FAKULTAS"
WHERE "NM_MK" LIKE '%is%';


13.
SELECT "ALAMAT", COUNT ("NM_DOSEN") AS "JML_DOSEN"
FROM "Dosen"
GROUP BY "ALAMAT"


14.
SELECT "NM_DOSEN", "NM_MK", "NM_JURUSAN", "NM_FAKULTAS", "NM_KELAS", "NM_RUANG"
FROM "Kelas" kls INNER JOIN "Dosen" dsn ON kls."KD_DOSEN" = dsn."KD_DOSEN" INNER JOIN "Matakuliah" mk ON kls."KD_MK" = mk."KD_MK"
INNER JOIN "Tabel_Ruang" rng ON kls."KD_RUANG" = rng."KD_RUANG" INNER JOIN "Jurusan" jrs ON mk."KD_JURUSAN" = jrs."KD_JURUSAN"
INNER JOIN "Fakultas" fk ON jrs."KD_FAKULTAS" = fk."KD_FAKULTAS"
ORDER BY "NM_DOSEN"


15.
SELECT "NM_DOSEN", COUNT ("NM_MK") AS "JML_MATAKULIAH"
FROM "Kelas" kls INNER JOIN "Dosen" dsn ON kls."KD_DOSEN" = dsn."KD_DOSEN"
INNER JOIN "Matakuliah" mk ON kls."KD_MK" = mk."KD_MK"
GROUP BY "NM_DOSEN"


16.
SELECT AVG ("KAPASITAS") AS "RATA2_KAPASITAS"
FROM "Tabel_Ruang"
FLOOR ("RATA2_KAPASITAS")


17.
SELECT COUNT ("NM_MHS") AS  "TOTAL_MHS", "NM_RUANG"
FROM "Kelas" kls FULL JOIN "Tabel_Ruang" rng ON kls."KD_RUANG" = rng."KD_RUANG"
FULL JOIN "Matakuliah" mk ON kls."KD_MK" = mk."KD_MK" 
FULL JOIN "Jurusan" jrs ON mk."KD_JURUSAN" = jrs."KD_JURUSAN"
FULL JOIN "Mahasiswa" mhs ON mhs."KD_JURUSAN" = jrs."KD_JURUSAN"
GROUP BY "NM_RUANG"


18.
SELECT MIN ("KAPASITAS")
FROM "Tabel_Ruang"


19.
SELECT MAX ("KAPASITAS")
FROM "Tabel_Ruang"


20.


21.
SELECT "NIM", "NM_MHS", "NM_JURUSAN", "NM_FAKULTAS"
FROM "Mahasiswa" mhs LEFT JOIN "Jurusan" jrs ON mhs."KD_JURUSAN" = jrs."KD_JURUSAN"
LEFT JOIN "Fakultas" fk ON jrs."KD_FAKULTAS" = fk."KD_FAKULTAS"


22.
SELECT "NIM", "NM_MHS", "NM_JURUSAN", "NM_FAKULTAS"
FROM "Mahasiswa" mhs LEFT JOIN "Jurusan" jrs ON mhs."KD_JURUSAN" = jrs."KD_JURUSAN"
LEFT JOIN "Fakultas" fk ON jrs."KD_FAKULTAS" = fk."KD_FAKULTAS"


23.


24.


25.
SELECT "NM_FAKULTAS", "NM_JURUSAN", COUNT ("NM_MHS") AS "JML_MHS"
FROM "Mahasiswa" mhs FULL JOIN "Jurusan" jrs ON mhs."KD_JURUSAN" = jrs."KD_JURUSAN"
FULL JOIN "Fakultas" fk ON jrs."KD_FAKULTAS" = fk."KD_FAKULTAS"
GROUP BY "NM_FAKULTAS", "NM_JURUSAN"
HAVING COUNT ("NM_MHS") = 0


26.
SELECT "ALAMAT", COUNT ("NM_MHS") AS "JML_MHS"
FROM "Mahasiswa"
GROUP BY "ALAMAT"


27.
SELECT "JK", COUNT ("NM_MHS") AS "JML_MHS"
FROM "Mahasiswa"
GROUP BY "JK"


28.
SELECT "JK", COUNT ("NM_MHS") AS "JML_MHS"
FROM "Mahasiswa"
GROUP BY "JK"


29.
SELECT COUNT ("NM_MHS") AS "JML_MHS"
FROM "Mahasiswa" 
WHERE "NM_MHS" LIKE '%Ratna%'


30.
SELECT mhs."NIM", "NM_MHS", "NM_JURUSAN", "NM_FAKULTAS", "NM_MK", "SKS", "NILAI"
FROM "Fakultas" fk FULL JOIN "Jurusan" jrs ON fk."KD_FAKULTAS" = jrs."KD_FAKULTAS"
FULL JOIN "Matakuliah" mk ON jrs."KD_JURUSAN" = mk."KD_JURUSAN"
FULL JOIN "Mahasiswa" mhs ON mk."KD_JURUSAN" = mhs."KD_JURUSAN"
FULL JOIN "Kelas" kls ON mk."KD_MK" = kls."KD_MK"
FULL JOIN "Kelas_Detail" dtl ON kls."KD_KELAS" = dtl."KD_KELAS"
ORDER BY "NIM"


31.
SELECT mhs."NIM", "NM_MHS", "NM_JURUSAN", "NM_FAKULTAS", "NM_MK", "SKS", "BOBOT"
FROM "Fakultas" fk FULL JOIN "Jurusan" jrs ON fk."KD_FAKULTAS" = jrs."KD_FAKULTAS"
FULL JOIN "Matakuliah" mk ON jrs."KD_JURUSAN" = mk."KD_JURUSAN"
FULL JOIN "Mahasiswa" mhs ON mk."KD_JURUSAN" = mhs."KD_JURUSAN"
FULL JOIN "Kelas" kls ON mk."KD_MK" = kls."KD_MK"
FULL JOIN "Kelas_Detail" dtl ON kls."KD_KELAS" = dtl."KD_KELAS"
FULL JOIN "Tabel_Bobot_Nilai" bn ON dtl."NILAI" = bn."KD_NILAI"
ORDER BY "NIM"


32.
SELECT mhs."NIM", mhs."NM_MHS", jrs."NM_JURUSAN", fk."NM_FAKULTAS", mk."NM_MK", mk."SKS", bn."BOBOT" AS "BOBOT_NILAI"
FROM "Fakultas" fk
JOIN "Jurusan" jrs ON fk."KD_FAKULTAS"= jrs."KD_FAKULTAS"
JOIN "Mahasiswa" mhs ON jrs."KD_JURUSAN"= mhs."KD_JURUSAN"
JOIN "Kelas_Detail" dtl ON mhs."NIM"= dtl."NIM"
JOIN "Kelas" kls ON  dtl."KD_KELAS"= dtl."KD_KELAS"
JOIN "Matakuliah" mk ON kls."KD_MK"= mk."KD_MK"
JOIN "Tabel_Bobot_Nilai" bn ON dtl."NILAI"= bn."KD_NILAI";


33.
SELECT mhs."NIM", mhs."NM_MHS", jrs."NM_JURUSAN", fk."NM_FAKULTAS", mk."NM_MK", mk."SKS", SUM(mk."SKS"*bn."BOBOT")/SUM(mk."SKS") AS "IPK"
FROM "Fakultas" fk
JOIN "Jurusan" jrs ON fk."KD_FAKULTAS"= jrs."KD_FAKULTAS"
JOIN "Mahasiswa" mhs ON jrs."KD_JURUSAN"= mhs."KD_JURUSAN"
JOIN "Kelas_Detail" dtl ON mhs."NIM"= dtl."NIM"
JOIN "Kelas" kls ON  dtl."KD_KELAS"= dtl."KD_KELAS"
JOIN "Matakuliah" mk ON kls."KD_MK"= mk."KD_MK"
JOIN "Tabel_Bobot_Nilai" bn ON dtl."NILAI"= bn."KD_NILAI"
GROUP BY mhs."NIM", jrs."NM_JURUSAN", fk."NM_FAKULTAS", mk."NM_MK", mk."SKS"


34.
SELECT mhs."NIM", "NM_MHS", "NM_JURUSAN", "NM_FAKULTAS", "NM_MK", "SKS", "NILAI", dtl."STATUS"
FROM "Fakultas" fk FULL JOIN "Jurusan" jrs ON fk."KD_FAKULTAS" = jrs."KD_FAKULTAS"
FULL JOIN "Matakuliah" mk ON jrs."KD_JURUSAN" = mk."KD_JURUSAN"
FULL JOIN "Mahasiswa" mhs ON mk."KD_JURUSAN" = mhs."KD_JURUSAN"
FULL JOIN "Kelas" kls ON mk."KD_MK" = kls."KD_MK"
FULL JOIN "Kelas_Detail" dtl ON kls."KD_KELAS" = dtl."KD_KELAS"
WHERE dtl."STATUS" = 'LULUS'
ORDER BY "NIM"


35.
SELECT mhs."NIM", "NM_MHS", "NM_JURUSAN", "NM_FAKULTAS", "NM_MK", "SKS", "NILAI", dtl."STATUS"
FROM "Fakultas" fk FULL JOIN "Jurusan" jrs ON fk."KD_FAKULTAS" = jrs."KD_FAKULTAS"
FULL JOIN "Matakuliah" mk ON jrs."KD_JURUSAN" = mk."KD_JURUSAN"
FULL JOIN "Mahasiswa" mhs ON mk."KD_JURUSAN" = mhs."KD_JURUSAN"
FULL JOIN "Kelas" kls ON mk."KD_MK" = kls."KD_MK"
FULL JOIN "Kelas_Detail" dtl ON kls."KD_KELAS" = dtl."KD_KELAS"
WHERE dtl."STATUS" = 'MENGULANG'
ORDER BY "NIM"