package hari06;

public class Adder02 {
	static int add(int a, int b) {
		return a+b;
	}
	
	static double add(double a, double b) {
		return a*b;
	}
	
	public static void main(String[] args) {
		System.out.println(Adder02.add(5, 5));
		
		System.out.println(Adder02.add(5.5, 5.5));
	}
}
