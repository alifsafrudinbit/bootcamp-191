package hari06;

public class Student extends Person {
	// list property
	public String major;
	public double grade;
	
	// constructor
	public Student() {
		super();
		System.out.println("Call Cunstructor Student");
	}
	
	// method
	public void info() {
		System.out.println("Id : "+super.id);
		System.out.println("Name : "+super.name);
		System.out.println("Major : "+this.major);
		System.out.println("Grade : "+this.major);
	}
	
	// method
	public void display() {
		super.display();
		System.out.println("Major : "+this.major);
		System.out.println("Grade : "+this.grade);
	}
}
