package hari06;

public class Person {
	public int id;
	public String name;
	public String gender;
	public String address;
	
	public Person() {
		System.out.println("Call Cunstructor Person");
	}
	
	public void display() {
		System.out.println("ID : "+this.id);
		System.out.println("Nama : "+this.name);
		System.out.println("Gender : "+this.gender);
		System.out.println("Address : "+this.address);
	}
}
