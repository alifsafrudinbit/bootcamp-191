package hari06;

public class Address {
	int id; 	
	String street;
	String city;
	String state;
	String country;
	int zipCode;
	
	public Address(int idE, String streetE, String cityE, String stateE, String countryE, int zipCodeE) {
		this.id = idE;
		this.street = streetE;
		this.city = cityE;
		this.state = stateE;
		this.country = countryE;
		this.zipCode = zipCodeE;
	}
}
