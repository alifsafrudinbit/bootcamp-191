package hari05;

public class Motor {
	int id;
	String motorName;
	String motorMerk;
	String motorSeries;
	int years;
	
	public Motor(int idMotor, String motorName, String motorMerk, String motorSeries, int years) {
		this.id = idMotor;
		this.motorName = motorName;
		this.motorMerk = motorMerk;
		this.motorSeries = motorSeries;
		this.years = years;
	}
	
	public void tampil() {
		System.out.println("ID Motor : "+this.id);
		System.out.println("Nama Motor : "+this.motorName);
		System.out.println("Merk Motor : "+this.motorMerk);
		System.out.println("Series Motor : "+this.motorSeries);
		System.out.println("Tahun Motor : "+this.years);
	}
}
