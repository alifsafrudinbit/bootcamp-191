package hari05;

public class Mobil {
	int idCar;
	String carName;
	String carMerk;
	String carSeries;
	int carYears;
		
	public Mobil(int idCar, String carName, String carMerk, String carSeries, int carYrs) {
		this.idCar = idCar;
		this.carName = carName;
		this.carMerk = carMerk;
		this.carSeries = carSeries;
		this.carYears = carYrs;
	}
	
	public void showData() {
		System.out.println("Id Mobil : " +this.idCar);
		System.out.println("Nama Mobil : "+this.carName);
		System.out.println("Merk Mobil : "+this.carMerk);
		System.out.println("Seri Mobil : "+this.carSeries);
		System.out.println("Tahun Mobil : "+this.carYears);
	}

}
