	package hari05;

public class Person01Main {
	public static void main(String[] args) {
		// initial by referencce
		Person01 person = new Person01();
		
		// initial by method
		// insert new data
		person.setId(1);
		person.setName("Alif Safrudin");
		person.setAddress("Klaten");
		person.setAge(25);
		
		// show data
		System.out.println("Id :"+person.getId());
		System.out.println("Nama : "+person.getName());
		System.out.println("Address : "+person.getAddress());
		System.out.println("Umur : "+person.getAge());
	}
	
}
