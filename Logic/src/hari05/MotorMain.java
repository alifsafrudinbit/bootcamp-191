package hari05;

public class MotorMain {
	public static void main(String[] args) {
		Motor mtr1 = new Motor(1, "CBR1000RR", "Honda", "Big Bike", 2018);
		System.out.println("Rincian Motor 1 : \n");
		mtr1.tampil();
		
		Motor mtr2 = new Motor(2, "ZX1000", "Kawasaki", "Big Bike", 2019);
		System.out.println("Rincian Motor 2 : \n");
		mtr2.tampil();
				
		Mobil mbl2 = new Mobil(2, "Pajero Sport", "Mitsubhisi", "Dakar", 2019);
		System.out.println("RIncian Mobil 2 : \n");
		mbl2.showData();
		
		Mobil mbl3 = new Mobil(3, "Fortuner", "Toyota", "TRD Sportivo", 2018);
		System.out.println("Rincian Mobil 3 : \n");
		mbl3.showData();
	}
}
