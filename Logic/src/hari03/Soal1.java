package hari03;

import java.util.Scanner;

import common.PrintArray;

public class Soal1 {
	static Scanner scn;
	public static void main(String[] args) {
		scn = new Scanner(System.in);
		System.out.println("Masukkan N : ");
		int n = scn.nextInt();
		System.out.println("Masukkan M : ");
		int m = scn.nextInt();
		System.out.println("Masukkan O : ");
		int o = scn.nextInt();
	
		// 1. Buat Array Deret
		int[] deret = new int[n*4];
		int angka = o;
		for (int i = 0; i < deret.length; i++) {
			if( i % 4 == 3) {
				deret[i] = m;
			}else {
				deret[i] = angka;
				angka += m;
			}
		}
		// 2. buat array 2 dimensi
		String[][] array = new String[n][n];
		// 4. membuat index
		int index=0;
		// 3. isi baris ke 0
		for (int i = 0; i < n; i++) {
			array[0][i]=deret[index]+""; //baris [0] tetap kolom berubah, dari kiri kekanan
			index++;
		}
		// 4. isi 
		for (int i = 1; i < n; i++) { 
			array[i][n-1]=deret[index]+""; //baris berubah kolom [7] tetap, dari atas kanan k bawah
			index++;
		}
		// 5.
		for (int i = n-2; i >= 0; i--) {
			array[n-1][i]=deret[index]+"";  //baris [7] tetap kolom berubah, dari bawah kanan ke kiri
			index++;
		}
		// 6.
		for (int i = n-2; i > 0; i--) {
			array[i][0]=deret[index]+"";
			index++;
		}
		
		// memanggil method print array2D
		PrintArray.array2D(array);
	}
}
