package hari03;

import java.util.Scanner;

import common.PrintArray;

public class Soal2 {
	static Scanner scn;  // properti scn
	public static void main(String[] args) { // method main
		scn = new Scanner(System.in);
		System.out.println("Masukkan N : ");
		int n = scn.nextInt();
		System.out.println("Masukkan M : ");
		int m = scn.nextInt();
		System.out.println("Masukkan O : ");
		int o = scn.nextInt();
		
		int[] deret = new int[n*4];
		int angka = o;
		for (int i = 0; i < deret.length; i++) {
			if (i % 4 == 3) {
				deret[i] = m;
				m *= 3;
			}else {
				deret[i] = o;
				o += 3;
			}
		}
		
		String[][] array = new String[n][n];
		int index = 0;
		// mengisi array
		for (int i = 0; i < n; i++) {
			array[n-1-i][i]=deret[index]+"";
			index++;
		}
		
		for (int i = 1; i < n; i++) {
			array[i][n-1]=deret[index]+"";
			index++;
		}
		
		for (int i = 5; i > 0; i--) {
			array[n-1][i]=deret[index]+"";  //baris [7] tetap kolom berubah, dari bawah kanan ke kiri
			index++;
		}
		PrintArray.array2D(array);	
	}
}
