package hari01;

public class IfCondition {
	public static void main(String[] args) {
		int x = 10;
		if(x / 5 == 2) {
			System.out.println(" x / 5 =>2 ");
		} else if ( x * 2 == 10) {
			System.out.println(" x * 2 => 10");
		} else if ( x % 2 == 0) {
			System.out.println(" x % 2 => 0");
		} else if ( x / 5 ==  2) {
			System.out.println(" x / 5 => 2");
		} else  {
			System.out.println("Else");
		}
	}
}
