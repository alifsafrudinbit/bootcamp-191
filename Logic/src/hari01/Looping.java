package hari01;

import java.util.Scanner; // 

public class Looping {
	// membuuat object scn
	static Scanner scn; // scn = objek
	
	public static void main(String[] args) {
		//  instansiasi
		scn = new Scanner(System.in);
		int a = 10;
		int b = 50;
		int hasilTambah = tambah(a,b);
		int hasilKali = kali(a,b);
		float hasilBagi = bagi(a,b);
		int hasilKurang  = kurang(a,b);
		
		System.out.println("Hasil tambah =" + hasilTambah );
		System.out.println("Hasil kali =" + hasilKali );
		System.out.println("Hasil bagi =" + hasilBagi );
		System.out.println("Hasil kurang =" + hasilKurang );	
	}
		
		
		static int tambah(int x, int y) {
			return x+y;
		}
		
		static int kali(int m, int y) {
			return m*y;
		}
		
		static float bagi(float i, float j) {
			return i/j;
		}
		
		static int kurang(int a, int b) {
			return a-b;
		}
		
		int n = scn.nextInt();
		
		static void inputOutput() {
		scn = new Scanner(System.in);
		System.out.println("Masukkan");
		String nama = scn.nextLine();
		System.out.println(" Nama Anda : " + nama);
		}
}
