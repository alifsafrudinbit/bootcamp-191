package latihan;

public class Super_class {
	int num = 20;
	
	// display method of superclass
	public void display() {
		System.out.println("This is display method of superclass");
	}
}

class Sub_class extends Super_class{
	int num = 10;
	
	// display method of subclass
	public void display() {
		System.out.println("This is display method of subclass");
	}
	
	public void my_method() {
		// instantiasi subclass
		Sub_class sub = new Sub_class();
		
		// memanggil method display() pada subclass
		sub.display();
		
		// memanggil method display() pada superclass
		super.display();
		
		System.out.println("value of the variable named num in sub class: " + sub.num);
		
		System.out.println("value of the variable named num in superclass: " + super.num);
	}
	
	public static void main(String[] args) {
		Sub_class obj = new Sub_class();
		obj.my_method();
	}
}


