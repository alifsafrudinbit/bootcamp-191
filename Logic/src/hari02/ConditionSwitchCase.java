package hari02;

import java.util.*;

public class ConditionSwitchCase {
	static Scanner scn;
	public static void main(String[] args) {
		scn = new Scanner(System.in);
		System.out.print("Masukkan Grade : ");
		char grade = scn.nextLine().charAt(0);
		
		switch (grade) {
		case 'A':
			System.out.println("Excelent");
			break;
		case 'B':
		case 'C':
			System.out.println("Well done");
			break;
		case 'D':
			System.out.println("You Passed");
			break;
		case 'E':
			System.out.println("Better try again");
			break;
		default:
			System.out.println("Invalid grade");
			break;
		}
	}
}
