package hari07;

public class InterfaceMain {
	public static void main(String[] args) {
		Bike02 h02 = new Honda02();
		Bike02 y02 = new Yamaha02();
		
		h02.run();
		y02.run();
		y02.speed();
		System.out.println(y02.speed());
	}
}
