package hackerrank.java;

import java.util.Scanner;

public class JavaLoop2 {
	static Scanner scnr;
	public static void main(String[] args) {
		//instansiasi
		scnr = new Scanner(System.in);
		//input
		int m =scnr.nextInt();
		for (int j = 1; j <=10; j++) {
			int kali = m*j;
			//output
			System.out.println(m+" x "+j+" = "+kali);
		}
	}
}
