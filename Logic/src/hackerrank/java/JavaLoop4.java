package hackerrank.java;

import java.util.Scanner;

public class JavaLoop4 {

	static Scanner sec;
	public static void main(String[] args) {
		//instansiasi dari calss Scanner
		sec = new Scanner(System.in);		
		//input 
		int p = sec.nextInt();
		for (int h = 1; h <=10; h++) {
			int perkalian = p*h;
			//output
			System.out.println(p+" x "+h+" = "+perkalian);
		}
	}
}
