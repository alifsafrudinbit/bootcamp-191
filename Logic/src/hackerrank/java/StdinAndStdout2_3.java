package hackerrank.java;

import java.util.Scanner;

public class StdinAndStdout2_3 {
	static Scanner input;
	public static void main(String[] args) {
		input = new Scanner(System.in);
		
		//input 
		int g = input.nextInt();
		double h =input.nextDouble();
		input.nextLine();
		String i = input.nextLine();
		
		//output
		System.out.println(g);
		System.out.println(h);
		System.out.println(i);
	}

}
