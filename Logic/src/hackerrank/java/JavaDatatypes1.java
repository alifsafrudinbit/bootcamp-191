package hackerrank.java;

import java.util.Scanner;

public class JavaDatatypes1 {
	static Scanner scn;
	public static void main(String[] args) {
		scn = new Scanner(System.in);
		int n = scn.nextInt();
		
		for (int i = 0; i < n; i++) {
			try {
				long x = scn.nextLong();
					System.out.println(x + "can be fitted in : ");
				
				if (x>=128 && x<=127) {
					System.out.println("* byte");
				}
				if (x>= (Math.pow(2, 15)*-1) && x<= Math.pow(2, 15)-1) {
					System.out.println("* short");
				}
				if (x>= (Math.pow(2, 31)*-1) && x<= Math.pow(2, 31)-1) {
					System.out.println("* int");
				}
				if (x>= (Math.pow(2, 36)*-1) && x<= Math.pow(2, 36)-1) {
					System.out.println("* long");
				}
			} catch (Exception e) {
				System.out.println(scn.next()+"can't be fitted anywhere.");
			}
			
		}
	}
}
