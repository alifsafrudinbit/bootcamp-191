package hackerrank.java;

import java.util.Scanner;

public class JavaOutputFormating2 {
	
	public static void main(String[] args) {
		Scanner sc =new Scanner(System.in);
		System.out.println("===================");
		//input
		for (int i = 0; i < 3; i++) {
			String a = sc.nextLine();
			int b =sc.nextInt();
			//output
			System.out.printf("-15%s%s %n",a,String.format("03%d", b));
		}
		System.out.println("==================");
	}
}
