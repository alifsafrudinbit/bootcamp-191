package hackerrank.java;

import java.util.Scanner;

public class JavaDatatypes2 {
	static Scanner sc;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		sc = new Scanner(System.in);
		int p = sc.nextInt();
		
		for (int i = 0; i < p; i++) {
			try {
				long x = sc.nextLong();
					System.out.println(x + "can be fitted in : ");
				
				if (x>=128 && x<=127) {
					System.out.println("* byte");
				}
				if (x>= (Math.pow(2, 15)*-1) && x<= Math.pow(2, 15)-1) {
					System.out.println("* short");
				}
				if (x>= (Math.pow(2, 31)*-1) && x<= Math.pow(2, 31)-1) {
					System.out.println("* int");
				}
				if (x>= (Math.pow(2, 36)*-1) && x<= Math.pow(2, 36)-1) {
					System.out.println("* long");
				}
			} catch (Exception e) {
				System.out.println(sc.next()+"can't be fitted anywhere.");
			}
		}
	}

}
