package hackerrank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock4 {

	static int e;
	static int f;
	static boolean flag = false;
	static final Scanner ase = new Scanner(System.in);
	
	static {
		e=ase.nextInt();
		f = ase.nextInt();
		if(e>0 && f>0) {
			flag = true;
		}else {
			System.out.println("java.lang.Exception: Breadth and hight mus be positive ");
		}
	}
	
	public static void main(String[] args) {
		if (flag) {
			int aer = e*f;
			System.out.println(aer);
		}
	}
}
