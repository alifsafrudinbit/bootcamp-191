package hackerrank.java;

import java.util.Scanner;

public class StdInAndStdout2_1 {
	static Scanner scan;
	public static void main(String[] args) {
		//instansiasi
		scan=new Scanner (System.in);
		
		//input
		int a = scan.nextInt();
		double b =scan.nextDouble();
		scan.nextLine();
		String c = scan.nextLine();
		
		//output
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
	}
}
