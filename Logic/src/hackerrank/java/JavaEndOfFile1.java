package hackerrank.java;

import java.util.Scanner;

public class JavaEndOfFile1 {
	static Scanner sc;
	public static void main(String[] args) {
		sc = new Scanner(System.in);
		int i = 1;
		while(sc.hasNext()) {
			System.out.println(i+" " +sc.nextLine());
			i++;
		}
	}
}
