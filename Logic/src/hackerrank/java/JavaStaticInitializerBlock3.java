package hackerrank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock3 {
	static int c;
	static int d;
	static boolean flag=false;
	static final Scanner asc = new Scanner(System.in);
	
	static {
		c = asc.nextInt();
		d = asc.nextInt();
		if (c>0 && d>0) {
			flag = true;
			
		}else {
			System.out.println("java.lang.Exception: Breadth and hight mus be positive ");
		}
	}
	public static void main(String[] args) {
		if (flag) {
			int are = c*d;
			System.out.println(are);
		}
	}
	
}
