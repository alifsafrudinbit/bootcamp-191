package hackerrank.java;

import java.util.Scanner;

public class JavaOutputFormating5 {

	public static void main(String[] args) {
		//instansiasi dari object Scanner
		Scanner scana =new Scanner(System.in);
		System.out.println("================");
		//input dengan for
		for (int h = 0; h < 3; h++) {
			String ss1 =scana.nextLine();
			int x3 =scana.nextInt();
			
			//output
			System.out.printf("%-15%s%s %n",ss1,String.format("%03d", x3));
		}
		
	}
	
}
