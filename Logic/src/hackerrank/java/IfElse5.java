package hackerrank.java;

import java.util.Scanner;

public class IfElse5 {
	static Scanner ipt;
	public static void main(String[] args) {
		ipt = new Scanner(System.in);
		int p = ipt.nextInt(); // instansiasi
		
		if(p%2==1) {
			System.out.println("Werid");
		}else {
			if(p>=2 && p<=5) {
				System.out.println("Not Werid");
			}else if(p>=6 && p<=20) {
				System.out.println("Werid");
			}else {
				System.out.println("Not Werid");
			}
		}
	}

}
