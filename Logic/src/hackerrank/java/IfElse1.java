package hackerrank.java;

import java.util.Scanner;

public class IfElse1 {
	static Scanner scan;
	public static void main(String[] args) {
		scan = new Scanner(System.in);
		int n = scan.nextInt();
		
		if(n%2 == 1) {
			System.out.println("Weird");
		}else {
			if (n>=2 && n<=5) {
				System.out.println("Not Weird");
			} else if(n>=6 && n<=20){
				System.out.println("Weird");
			}else {
				System.out.println("Not Weird");
			}
		}
	}
}
