package hackerrank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock5 {
	static int g;
	static int h;
	static boolean flag = true;
	
	static final Scanner ade = new Scanner(System.in);
	static {
		g=ade.nextInt();
		h=ade.nextInt();
		if(g>0 && h>0) {
			flag = true;
		}else {
			System.out.println("java.lang.Exception: Breadth adn hight mus be positive ");
		}
	}
	public static void main(String[] args) {
		if(flag) {
			int ara = g*h;
			System.out.println(ara);
		}
	}
}
