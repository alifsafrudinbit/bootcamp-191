package hackerrank.java;

import java.util.Scanner;

public class StdinAndStdout2_5 {
	static Scanner scaner;
	public static void main(String[] args) {
		//instansiasi
		scaner = new Scanner(System.in);
		
		//input 
		int m =scaner.nextInt();
		double n =scaner.nextDouble();
		scaner.nextLine();
		String o =scaner.nextLine();
		
		//output
		System.out.println(m);
		System.out.println(n);
		System.out.println(o);
	}
}
