package hackerrank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock1 {
	static int b;
	static int h;
	static Boolean flag =false;	
	static final Scanner scn = new Scanner(System.in);
	static {
		b=scn.nextInt();
		h=scn.nextInt();
		if (b>0 && h>0) {
			flag =true;
		}else {
			System.out.println("java.lang.Exception: Breadth and hight must be positive");
			
		}
	}
	
	public static void main(String[] args) {
		
		if(flag) {
			int area =b*h;
			System.out.print(area);
		}
	}

}
