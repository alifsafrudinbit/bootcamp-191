package hackerrank.java;

import java.util.Scanner;

public class JavaEndOfFile2 {
	static Scanner cpy;
	public static void main(String[] args) {
		cpy = new Scanner(System.in);
		int x = 1;
		while(cpy.hasNext()) {
			System.out.println(x+" "+cpy.nextLine());
			x++;
		}
	}

}
