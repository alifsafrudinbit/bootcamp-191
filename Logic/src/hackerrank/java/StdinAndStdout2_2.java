package hackerrank.java;

import java.util.Scanner;

public class StdinAndStdout2_2 {
	static Scanner masuk;
	public static void main(String[] args) {
		masuk = new Scanner(System.in);
		
		//input
		int d = masuk.nextInt();
		double e = masuk.nextDouble();
		masuk.nextLine();
		String f = masuk.nextLine();
		
		//output
		System.out.println(d);
		System.out.println(e);
		System.out.println(f);
	}
}
