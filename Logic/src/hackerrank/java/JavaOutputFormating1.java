package hackerrank.java;

import java.util.Scanner;

public class JavaOutputFormating1 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("=====================");
		//inputan
		for (int i = 0; i < 3; i++) {
			String s1 = scan.next();
			int x = scan.nextInt();
			
			//output
			System.out.printf("%-15s%s %n",s1,String.format("%03d",	x));
		}
		System.out.println("======================");
		
	}

	
}
