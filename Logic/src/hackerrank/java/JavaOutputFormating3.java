package hackerrank.java;

import java.util.Scanner;

public class JavaOutputFormating3 {

	public static void main(String[] args) {
		Scanner sc2 =new Scanner(System.in);
		System.out.println("=================");
		//perulangan input
		for (int i = 0; i < 3; i++) {
			//input
			String st = sc2.nextLine();
			int ss =sc2.nextInt();
			//output
			System.out.printf("-15%s%s %n",st,String.format("%03d", ss));
		}
		System.out.println("=================");
	}
}
