package hackerrank.java;

import java.util.Scanner;

public class JavaLoop1 {
	static Scanner sc;
	public static void main(String[] args) {
		//instansiasi scanner
		sc = new Scanner(System.in);
		int n =sc.nextInt();
		for (int i = 1; i <=10; i++) {
			int result = n*i;
			System.out.println(n+" x "+i+" = "+result);
		}
	}
}
