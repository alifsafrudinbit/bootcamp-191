package hackerrank.java;

import java.util.Scanner;

//class adalah blue print dari object
public class JavaStdinAndStdout1_2 {
	static Scanner scan;//properti dari class

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		scan = new Scanner(System.in);//instansiasi dari scanner
		
		int a = scan.nextInt();//variabel inputan
		int b = scan.nextInt();
		int c = scan.nextInt();
		
		System.out.println(a);//cetak
		System.out.println(b);
		System.out.println(c);
		
	}

}
