package hackerrank.java;

import java.util.Scanner;

public class StdinAndStdout2_4 {
	static Scanner scan;
	public static void main(String[] args) {
		//instansiasi
		scan =new Scanner(System.in);
		
		//input
		int j = scan.nextInt();
		double k =scan.nextDouble();
		scan.nextLine();
		String l =scan.nextLine();
		
		//output
		System.out.println(j);
		System.out.println(k);
		System.out.println(l);
				
	}
}
