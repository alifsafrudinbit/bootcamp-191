package hackerrank.java;

import java.util.Scanner;

public class JavaEndOfFile5 {
	static Scanner scns;
	public static void main(String[] args) {
		scns = new Scanner (System.in);
		int o = 1;
		while(scns.hasNext()) {
			System.out.println(o+" "+scns.nextLine());
			o++;
		}
	}
}
