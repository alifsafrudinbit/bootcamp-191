package hackerrank.java;

import java.util.Scanner;

public class JavaEndOfFile3 {
	static Scanner ins;
	public static void main(String[] args) {
		ins = new Scanner(System.in);
		int a = 1;
		
		while(ins.hasNext()) {
			System.out.println(a+" "+ins.nextLine());
			a++;
		}
	}
}
