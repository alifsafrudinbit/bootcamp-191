package hackerrank.implementation;

import java.util.Arrays;

public class SockMerchant3 {
	static int shockMerchant(int l, int[] array) {
		Arrays.sort(array);
		int htg = 0;
		for (int i = 0; i < l-1; i++) {
			if (array[i] == array[i+1]) {
				htg = htg+1;
				i=i+1;
			}
		}
		return htg;
	}
	
	public static void main(String[] args) {
		int pjg = 20;
		int[] arr = {4,5,5,5,6,6,4,1,4,4,3,6,6,3,6,1,4,5,5,5};
		int pairs = shockMerchant(pjg, arr);
		System.out.println(pairs);
	}
}
