package hackerrank.implementation;

public class Kangaroo3 {
	static String kangaroo(int p1, int l1, int p2, int l2) {
		String result = "";
		if (p1 < p2 && l1 < l2) {
			result = "NO";
		}else {
			if (l1 != l2 && (p2 - p1) % (l1 - l2) == 0) {
				result = "YES";
			}else {
				result = "NO";
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		int p1 = 14;
		int l1 = 4;
		int p2 = 98;
		int l2 = 2;
		String meet = kangaroo(p1, l1, p2, l2);
		System.out.println("Apakah kangaroo 1 & 2 bertemu : "+meet);
	}
}
