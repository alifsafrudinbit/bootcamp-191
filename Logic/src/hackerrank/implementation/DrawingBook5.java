package hackerrank.implementation;

public class DrawingBook5 {

	static int countPage(int nC, int pC) {
		int h1 = Math.abs(pC/2);
		if (nC%2 == 0) {
			nC++;
		}
		int h2 = Math.abs((pC-nC)/2);
		return h1 < h2 ? h1 : h2;
	}
	
	public static void main(String[] args) {
		int nCountP = 4;
		int pCountP = 9;
		int hCountP = countPage(nCountP, pCountP);
		System.out.println(hCountP);
	}
}
