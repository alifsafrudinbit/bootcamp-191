package hackerrank.implementation;

public class Kangaroo5 {

	static String kangaroo(int xKa1, int vKa1, int xKa2, int vKa2) {
		String hsl = "";
		if (xKa1 < xKa2 && vKa1 < vKa2) {
			hsl = "NO";
		}else {
			if (vKa1 != vKa2 && (xKa2 - xKa1) % (vKa1 - vKa2) == 0) {
				hsl = "YES";
			}else {
				hsl = "NO";
			}
		}
		return hsl;
	}
		
	public static void main(String[] args) {
		int xKa1 = 28;
		int vKa1 = 8;
		int xKa2 = 96;
		int vKa2 = 2;
		String hasil = kangaroo(xKa1, vKa1, xKa2, vKa2);
		System.out.println("Apakah kangaroo bertemu : " +hasil);
	}
}
