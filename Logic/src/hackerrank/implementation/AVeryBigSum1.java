package hackerrank.implementation;

public class AVeryBigSum1 {
	static long aVeryBigSum1(long[] ar) {
		long b = 0;
		for (int i = 0; i < ar.length; i++) {
			b += ar[i];
		}
		return b;
	}
	
	public static void main(String[] args) {
		long[] ar = {10,20,30,40,50};
		long k = aVeryBigSum1(ar);
		System.out.println(k);
	}
}
