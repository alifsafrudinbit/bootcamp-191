package hackerrank.implementation;

public class GradingStudents5 {
	
	static int[] gradingStudents(int[] grades) {
		for (int i = 0; i < grades.length; i++) {
			if (grades[i] >= 38) {
				if (grades[i] + (5-grades[i]%5) - grades[i]<3) {
					grades[i] = grades[i] + (5-grades[i]%5);
				}
			}
		}
		return grades;
	}
	
	public static void main(String[] args) {
		int[] m = {56,78,23,179};
		int[] k = gradingStudents(m);
		System.out.println(k[0]);
		System.out.println(k[1]);
		System.out.println(k[2]);
		System.out.println(k[3]);
	}
}
