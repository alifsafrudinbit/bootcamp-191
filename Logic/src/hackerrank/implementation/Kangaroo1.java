package hackerrank.implementation;

public class Kangaroo1 {
	static String kangaroo(int x1, int v1, int x2, int v2) {
		String result = "";
		if (x1 < x2 && v1 < v2) {
			result = "NO";
		}else {
			if (v1 != v2 && (x2 - x1) % (v1 - v2) == 0) {
				result = "YES";
			}else {
				result = "NO";
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		int t1 = 0;
		int l1 = 3;
		int t2 = 4;
		int l2 = 2;
		String h = kangaroo(t1, l1, t2, l2);
		System.out.println(h);
	}
}
