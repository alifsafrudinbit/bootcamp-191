package hackerrank.implementation;

import java.util.Arrays;

public class SockMerchant2 {
	static int shockMerchant(int n, int[] arr) {
		Arrays.sort(arr);
		int count = 0;
		for (int i = 0; i < n-1; i++) {
			if(arr[i]==arr[i+1]) {
				count = count+1;
				i=i+1;
			}
		}
		return count;
	}
	
	public static void main(String[] args) {
		int length = 15;
		int[] shock = {6,5,2,3,5,2,2,1,1,5,1,3,3,3,5};
		int hasil = shockMerchant(length, shock);
		System.out.println(hasil);
	}
}
