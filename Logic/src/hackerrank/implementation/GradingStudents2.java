package hackerrank.implementation;

public class GradingStudents2 {

	static int[] gradingStudents2(int[] grades) {
		for (int i = 0; i < grades.length; i++) {
			if (grades[i] >= 38) {
				if (grades[i] + (5-grades[i]%5)-grades[i]<3) {
					grades[i] = (grades[i] + (5-grades[i]%5));
				}
			}
		}
		return grades;
	}
	
	public static void main(String[] args) {
		int[] input = {37,38};
		int[] output = gradingStudents2(input);
		System.out.println(output[0]);
		System.out.println(output[1]);
	}
}
