package hackerrank.implementation;

import java.util.ArrayList;
import java.util.List;

public class BonAppetit1 {
	
	static void bonAppetit(List<Integer> bill, int k, int b) {
		int c = 0;
		for (int i = 0; i < bill.size(); i++) {
			if (i!=k) {
				c = c+bill.get(i);
			}
		}
		int s = c/2;
		String rs1 = "";
		int rs2 = 0;
		if (b-s <= 0) {
			rs1 = " Bon Appetit";
		}else {
			rs2 = (b-s);
		}
	}
	
	public static void main(String[] args) {
		List<Integer> bill = new ArrayList<Integer>();
		bill.add(3);
		bill.add(10);
		bill.add(2);
		bill.add(9);
		
		int n = 4;
		int k = 1;
		int b = 12;
		
		for (Integer item : bonAppetit(bill, k, b)) {
			System.out.println(item);
		}

		
	}
}