package hackerrank.implementation;

public class AveryBigSum2 {
	static long aVeryBigSum2(long[] ar) {
		long a = 0;
		for (int i = 0; i < ar.length; i++) {
			a += ar[i];
		}
		return a;
	}
	
	public static void main(String[] args) {
		long[] ar = {100,200,300,400,500,600};
		long z = aVeryBigSum2(ar);
		System.out.println(z);
	}
}
