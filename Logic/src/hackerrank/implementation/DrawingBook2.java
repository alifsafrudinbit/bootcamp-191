package hackerrank.implementation;

public class DrawingBook2 {

	static int countPage(int p, int h) {
		int page1 = Math.abs(h/2);
		if(p%2 == 0){
		p++;
		}
		int page2 = Math.abs((h-p)/2);
		return page1 < page2 ? page1 : page2;
	}
	
	public static void main(String[] args) {
		int p = 5;
		int h = 4;
		int hsl = countPage(p, h);
		System.out.println(hsl);
	}
}
