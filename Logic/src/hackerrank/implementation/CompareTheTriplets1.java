package hackerrank.implementation;

import java.util.ArrayList;
import java.util.List;

public class CompareTheTriplets1 {
	static List<Integer> compareTheTriplets1(List<Integer> a, List<Integer> b){
		List<Integer> result = new ArrayList<Integer>();
		
		result.add(0);
		result.add(0);
		int nA = 0;
		int nB = 0;
		for (int i = 0; i < a.size(); i++) {
			if(a.get(i) > b.get(i)) {
				nA++;
				result.set(0, nA);
			}
			
			if(a.get(i) < b.get(i)) {
				nB++;
				result.set(1, nB);
			}
		}
		return result;
	}
	
	
	public static void main(String[] args) {
		List<Integer> d = new ArrayList<Integer>();
		d.add(12);
		d.add(15);
		d.add(14);
		
		List<Integer> e = new ArrayList<Integer>();
		e.add(12);
		e.add(15);
		e.add(14);
		
		for (Integer hasil : compareTheTriplets1(d, e)) {
			System.out.print(hasil + "\t");
		}
	}
}
