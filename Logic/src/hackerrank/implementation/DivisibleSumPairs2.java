package hackerrank.implementation;

public class DivisibleSumPairs2 {

	static int divisibleSumPairs(int n, int k, int[] arr) {
		int hasil = 0;
		for (int i = 0; i < arr.length; i++) {
			for (int j = i+1; j < arr.length; j++) {
				if ((arr[i]+arr[j])%k == 0) {
					hasil++;
				}
			}
		}
		return hasil;
	}
	
	public static void main(String[] args) {
		int k = 2;
		int l = 2;
		int[] a = {8,10};
		int h = divisibleSumPairs(k, l, a);
		System.out.println(h);
	}
}
