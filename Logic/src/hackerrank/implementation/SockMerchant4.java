package hackerrank.implementation;

import java.util.Arrays;

public class SockMerchant4 {
	static int sockMerchant(int n, int[] ar) {
	Arrays.sort(ar);
	int cnt = 0;
	for (int i = 0; i < n-1; i++) {
		if (ar[i] == ar[i+1]) {
			cnt = cnt+1;
			i = i+1;
		}
	}
	return cnt;
	}
	
	public static void main(String[] args) {
		int pj = 1;
		int[] a = {100};
		int hs = sockMerchant(pj, a);
		System.out.println(hs);
	}
}
