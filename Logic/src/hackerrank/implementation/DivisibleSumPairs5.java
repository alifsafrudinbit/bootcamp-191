package hackerrank.implementation;

public class DivisibleSumPairs5 {

	static int divisibleSumPairs(int n, int k, int[] arr) {
		int has = 0;
		for (int i = 0; i < arr.length; i++) {
			for (int j = i+1; j < arr.length; j++) {
				if ((arr[i]+arr[j])%k == 0) {
					has++;
				}
			}
		}
		return has;
	}
	
	public static void main(String[] args) {
		int nD = 10;
		int kD = 3;
		int[] aD = {29,97,52,86,27,89,77,19,99,96};
		int rD = divisibleSumPairs(nD, kD, aD);
		System.out.println(rD);
	}
}
