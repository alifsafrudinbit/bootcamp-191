package hackerrank.implementation;

public class DrawingBook1 {

	static int pageCount(int n, int p) {
		int page1 = Math.abs(p/2);
		if(n%2 == 0) {
			n++;
		}
		int page2 = Math.abs((p-2)/2);
		return page1 < page2 ? page1 : page2;
	}
	
	public static void main(String[] args) {
		int nPC = 5;
		int pPC = 4;
		int result = pageCount(nPC, pPC);
		System.out.println(result);
	}
}
