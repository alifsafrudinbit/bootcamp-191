package hackerrank.implementation;

import java.util.ArrayList;
import java.util.List;

public class CompareTheTriplets4 {
	static List<Integer> compareTheTriplets4(List<Integer> a, List<Integer> b){
		List<Integer> result = new ArrayList<Integer>();
		result.add(0);
		result.add(0);
		int alice = 0;
		int bob = 0;
		for (int i = 0; i < a.size(); i++) {
			if (a.get(i) > b.get(i)) {
				alice++;
				result.set(0, alice);
			}
			if (a.get(i) < b.get(i)) {
				bob++;
				result.set(1, bob);
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		List<Integer> i = new ArrayList<>();
		i.add(11);
		i.add(22);
		i.add(33);
		
		List<Integer> j = new ArrayList<>();
		j.add(33);
		j.add(22);
		j.add(11);
		
		for (Integer comp : compareTheTriplets4(i, j)) {
			System.out.println(comp + "\n");
		}
	}
}
