package hackerrank.implementation;

public class Kangaroo2 {
	static String kangaroo(int t1, int l1, int t2, int l2) {
		String result = "";
		if (t1 < t2 && l1 < l2) {
			result = "NO";
		}else {
			if(l1 != l2 && (t2 - t1) % (l1 - l2) == 0) {
				result = "YES";
			}else {
				result = "NO";
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		int mu1 = 0;
		int lo1 = 2;
		int mu2 = 5;
		int lo2 = 3;
		String hsl = kangaroo(mu1, lo1, mu2, lo2);
		System.out.println(hsl);
	}
}
