package hackerrank.implementation;

public class AVeryBigSum3 {
	static long aVeryBigSum(long[] ar) {
		long a = 0;
		for (int i = 0; i < ar.length; i++) {
			a += ar[i];
		}
		return a;
	}
	
	public static void main(String[] args) {
		long[] ar = {23,34,45,56,67};
		long x = aVeryBigSum(ar);
		System.out.println(x);
	}
}
