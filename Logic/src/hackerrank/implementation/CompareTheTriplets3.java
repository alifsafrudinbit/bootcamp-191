	package hackerrank.implementation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CompareTheTriplets3 {
	static List<Integer> compareTheTriplets3(List<Integer> a, List<Integer> b){
		List<Integer> result = new ArrayList<Integer>();
		result.add(0);
		result.add(0);
		int nAl = 0;
		int nBo = 0;
		for (int i = 0; i < a.size(); i++) {
			if (a.get(i) > b.get(i)) {
				nAl++;
				result.set(0, nAl);
			}
			if (a.get(i) < b.get(i)) {
				nBo++;
				result.set(1, nBo);
			}
		}
		return result;
	}
	
	public static void main(String[] args)throws IOException{
		List<Integer> h =  new ArrayList<Integer>();
		h.add(1);
		h.add(2);
		h.add(3);
		
		List<Integer> i  = new ArrayList<>();
		i.add(0);
		i.add(1);
		i.add(2);
		
		for (Integer kom : compareTheTriplets3(h, i)) {
			System.out.println(kom + "\t");
		}
	}
}
