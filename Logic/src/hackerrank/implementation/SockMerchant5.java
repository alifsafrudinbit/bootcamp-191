package hackerrank.implementation;

import java.util.Arrays;

public class SockMerchant5 {
	static int sockMerchant(int n, int[] arr) {
		Arrays.sort(arr);
		int result = 0;
		for (int i = 0; i < n-1; i++) {
			if (arr[i] == arr[i+1]) {
				result = result+1;
				i = i+1;
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		int nSM = 3;
		int[] aSM = {10,20,30};
		int pSM = sockMerchant(nSM, aSM);
		System.out.println(pSM);
	}
}
