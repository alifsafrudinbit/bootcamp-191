package hackerrank.implementation;

public class GradingStudents4 {

	static int[] gradingStudents(int[] grades) {
		for (int i = 0; i < grades.length; i++) {
			if (grades[i] >= 38) {
				if (grades[i] + (5-grades[i]%5) - grades[i]<3) {
					grades[i] = grades[i] + (5-grades[i]%5);
				}
			}
		}
		return grades;
	}
	
	public static void main(String[] args) {
		int[] mas = {87,56,89,34};
		int[] kel = gradingStudents(mas);
		System.out.println(kel[0]);
		System.out.println(kel[1]);
		System.out.println(kel[2]);
		System.out.println(kel[3]);
	}
}
