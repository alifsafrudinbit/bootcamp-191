package hackerrank.implementation;

import java.util.Arrays;

public class SockMerchant1 {
	static int sockMerchant(int n, int[] ar) {
		Arrays.sort(ar);
		int c = 0;
		for (int i = 0; i < n-1; i++) {
			if (ar[i]==ar[i+1]) {
				c = c+1;
				i = i+1;
			}
		}
		return c;
	}
	
	public static void main(String[] args) {
		int n = 9;
		int[] ar = {10,20,20,10,10,30,50,10,20};
		int h = sockMerchant(n, ar);
		System.out.println(h);
	}
}
