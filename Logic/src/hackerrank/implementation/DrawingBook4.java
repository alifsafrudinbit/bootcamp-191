package hackerrank.implementation;

public class DrawingBook4 {

	static int countPage(int n, int p) {
		int hal1 = Math.abs(p/2);
		if (n%2 == 0) {
			n++;
		}
		int hal2 = Math.abs((p-n)/2);
		return hal1 < hal2 ? hal1 : hal2;
	}
	
	public static void main(String[] args) {
		int nC = 7;
		int pC = 3;
		int hC = countPage(nC, pC);
		System.out.println(hC);
	}
}
