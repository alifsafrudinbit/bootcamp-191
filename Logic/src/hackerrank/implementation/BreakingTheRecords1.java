package hackerrank.implementation;

public class BreakingTheRecords1 {
	static int[] breakingRecord(int[] scores) {
		int[] result= new int[2];
		int minV=scores[0]; int minC = 0;
		int maxV=scores[0]; int maxC = 0;
		for (int i = 0; i < scores.length; i++) {
			if(scores[i]<minV) {
				minC++;
				minV=scores[i];
			}
			if(scores[i]>maxV) {
				maxC++;
				maxV=scores[i];
			}
		}
		result[0]=maxC;
		result[1]=minC;
		return result;
	}
	
	public static void main(String[] args) {
		int[] scores = {3,4,21,36,10,28,35,5,24,42};
		int[] result = breakingRecord(scores);
		System.out.print(result[0]+ "\t");
		System.out.println(result[1]);
	}
}
