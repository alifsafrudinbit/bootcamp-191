package hackerrank.implementation;

public class DrawingBook3 {

	static int countPage(int n, int p) {
		int page1 = Math.abs(p/2);
		if(n%2 == 0) {
			n++;
		}
		int page2 = Math.abs((p-n)/2);
		return page1 < page2 ? page1 : page2;
	}
	
	public static void main(String[] args) {
		int pj = 5;
		int hl = 5;
		int hs = countPage(pj, hl);
		System.out.println(hs);
	}
}
