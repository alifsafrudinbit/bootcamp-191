package hackerrank.implementation;

public class BreakingTheRecords2 {
	static int[] breakingTheRecord(int[] scores) {
		int[] result = new int[2]; 
		int minI = scores[0];
		int maxI = scores[0];
		int minJ = 0;
		int maxJ = 0;
		for (int i = 0; i < scores.length; i++) {
			if (scores[i]<minI) {
				minJ++;
				minI = scores[i];
			}
			if (scores[i]>maxI) {
				maxJ++;
				maxI = scores[i];
			}
		}
		result[0]=maxJ;
		result[1]=minJ;
		return result;
	}
	
	public static void main(String[] args) {
		int[] nilai = {100,45,41,60,17,41,45,43,100,40,89,92,34,6,64,7,37,81,32,50};
		int[] result = breakingTheRecord(nilai);
		System.out.print(result[0] + "\t");
		System.out.println(result[1]);
	}
}
