package hackerrank.implementation;

public class AVeryBigSum5 {
	static long aVeryBigSum(long[] ar) {
		long a = 0;
		for (int i = 0; i < ar.length; i++) {
			a += ar[i];
		}
		return a;
	}
	
	public static void main(String[] args) {
		long[] ar = {45,56,67,78,8,9};
		long l = aVeryBigSum(ar);
		System.out.println(l);
	}
}
