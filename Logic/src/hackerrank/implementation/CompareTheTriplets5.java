package hackerrank.implementation;

import java.util.ArrayList;
import java.util.List;

public class CompareTheTriplets5 {
	static List<Integer> compareTheTriplets5(List<Integer> a, List<Integer> b){
		List<Integer> result = new ArrayList<Integer>();
		result.add(0);
		result.add(0);
		int n1 = 0;
		int n2 = 0;
		for (int i = 0; i < a.size(); i++) {
			if (a.get(i) > b.get(i)) {
				n1++;
				result.set(0, n1);
			}
			if (a.get(i) < b.get(i)) {
				n2++;
				result.set(1, n2);
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		List<Integer> k = new ArrayList<>();
		k.add(2000);
		k.add(9000);
		k.add(7000);
		
		List<Integer> l = new ArrayList<>();
		l.add(5000);
		l.add(4000);
		l.add(6000);
		
		for (Integer komparasi : compareTheTriplets5(k, l)) {
			System.out.print(komparasi + "\t");
		}
	}
}
