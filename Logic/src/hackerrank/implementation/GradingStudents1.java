package hackerrank.implementation;

public class GradingStudents1 {
	static int[] gradingStudents(int[] grades) {
		for (int i = 0; i < grades.length; i++) {
			if (grades[i] >=38) {
				if (grades[i]+(5-grades[i]%5)-grades[i]<3) {
					grades[i] = (grades[i]+(5-grades[i]%5));
				}
			}
		}
		return grades;
	}
	
	public static void main(String[] args) {
		int[] i = {73,67,38,33};
		int[] o = gradingStudents(i);
		System.out.println(o[0]);
		System.out.println(o[1]);
		System.out.println(o[2]);
		System.out.println(o[3]);
	}
	
}
