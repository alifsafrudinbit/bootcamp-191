package hackerrank.implementation;

public class AppleAndOrange1 {
	static int countApplesAndOrange1(int s, int t, int a, int b, int[] apples, int[] oranges ) {
		// jumlah apple jatuh
		int cApple = 0;
		for (int i = 0; i < apples.length; i++) {
			int jarak = a+apples[i];
			// if jarak antara s & t
			if(jarak >= s && jarak <= t) {
				cApple++;
			}
		}
		// jml jeruk jatuh
		int cOrange = 0;
		for (int i = 0; i < oranges.length; i++) {
			int jarak = b+oranges[i];
			if(jarak >= s && jarak <= t) {
				cOrange++;
			}
		}
		return cApple;
		
	}
	
	public static void main(String[] args) {
		int[] apples = {2,9,7,9};
		int[] oranges = {3,6,5,7};
		int  f= 7, g= 10, h = 5, i = 15;
		int cA = countApplesAndOrange1(f, g, h, i, apples, oranges);
		System.out.println(cA);
		int cO = countApplesAndOrange1(f, g, h, i, apples, oranges);
		System.out.println(cO);
	}
}
