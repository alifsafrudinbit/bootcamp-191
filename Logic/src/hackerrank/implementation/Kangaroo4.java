package hackerrank.implementation;

public class Kangaroo4 {

	static String kangaroo(int tk1, int vk1, int tk2, int vk2) {
		String hasil = "";
		if (tk1 < tk2 && vk1 < vk2) {
			hasil = "NO";
		}else {
			if (vk1 != vk2 && (tk2 - tk1) % (vk1 - vk2) == 0) {
				hasil = "YES";
			}else {
				hasil = "NO";
			}
		}
		
		return hasil;
	}
	
	public static void main(String[] args) {
		int tk1 = 21;
		int vk1 = 6;
		int tk2 = 47;
		int vk2 = 3;
		String hasil = kangaroo(tk1, vk1, tk2, vk2);
		System.out.println("Apakah kangaroo 1 & 2 bertemu : "+hasil);
	}
}
