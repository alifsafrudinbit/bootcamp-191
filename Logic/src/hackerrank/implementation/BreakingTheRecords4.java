package hackerrank.implementation;

public class BreakingTheRecords4 {
	static int[] breakingTheRecords(int[] nilai) {
		int[] result = new int[2];
		int minNi = nilai[0];
		int maxNi = nilai[0];
		int minP = 0;
		int maxP = 0;
		for (int i = 0; i < nilai.length; i++) {
			if (nilai[i] < minNi) {
				minP++;
				minNi = nilai[i];
			}
			if (nilai[i] > maxNi) {
				maxP++;
				maxNi = nilai[i];
			}
		}
		result[0]=maxP;
		result[1]=minP;
		return result;
	}
	
	public static void main(String[] args) {
		int[] a = {124768,1256787,8754789,8372678,1111111,8463488,8326782};
		int[] result = breakingTheRecords(a);
		System.out.print(result[0] + "\t");
		System.out.println(result[1]);
	}
}
