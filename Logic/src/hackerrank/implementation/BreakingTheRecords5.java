package hackerrank.implementation;

public class BreakingTheRecords5 {
	static int[] breakingTheRecords(int[] nilai) {
		int[] result = new int[2];
		int nMax = nilai[0];
		int nMin = nilai[0];
		int jMax = 0;
		int jMin = 0;
		for (int j = 0; j < nilai.length; j++) {
			if (nilai[j] > nMax) {
				jMax++;
				nMax = nilai[j];
			}
			if (nilai[j] < nMin) {
				jMin++;
				nMin = nilai[j];
			}
		}
		result[0] = jMax;
		result[1] = jMin;
		return result;
	}
	
	public static void main(String[] args) {
		int[] sc = {567876,478563,289365,348673,458673,784658};
		int[] hsl = breakingTheRecords(sc);
		System.out.print(hsl[0]+"\t");
		System.out.println(hsl[1]);
	}
}
