package hackerrank.implementation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CompareTheTriplets2 {
	static List<Integer> compareTheTriplets(List<Integer> a, List<Integer> b){
		List<Integer> result = new ArrayList<Integer>();
		result.add(0);
		result.add(0);
		
		int nAlice = 0;
		int nBob = 0;
		for (int i = 0; i < a.size(); i++) {
			if (a.get(i) > b.get(i)) {
				nAlice++;
				result.set(0, nAlice);
			}
			
			if (a.get(i) < b.get(i)) {
				nBob++;
				result.set(1, nBob);
			}
		}
		return result;
	}
	
	public static void main(String[] args) throws IOException {
		List<Integer> f = new ArrayList<Integer>();
		f.add(5);
		f.add(6);
		f.add(7);
		
		List<Integer> g = new ArrayList<Integer>();
		g.add(6);
		g.add(7);
		g.add(8);
		
		for (Integer compare : compareTheTriplets(f, g)) {
			System.out.print(compare + "\t");
		}
	}
	
}
