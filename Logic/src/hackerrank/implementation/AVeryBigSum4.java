package hackerrank.implementation;

public class AVeryBigSum4 {
	static long aVeryBigSum5(long[] ar) {
		long a = 0;
		for (int i = 0; i < ar.length; i++) {
			a += ar[i];
		}
		return a;
	}
	
	public static void main(String[] args) {
		long[] ar = {90,89,78,67,56,45,34};
		long o = aVeryBigSum5(ar);
		System.out.println(o);
	}
}
