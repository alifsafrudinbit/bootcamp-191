package hackerrank.implementation;

public class GradingStudents3 {

	static int[] gradingStudents(int[] grades) {
		for (int i = 0; i < grades.length; i++) {
			if (grades[i] >=38) {
				if (grades[i] + (5-grades[i]%5)-grades[i]<3) {
					grades[i] = grades[i] + (5-grades[i]%5);
				}
			}
		}
		return grades;
	}
	
	public static void main(String[] args) {
		int[] in = {57,98,56,81,38,30,1,0};
		int[] out = gradingStudents(in);
		System.out.println(out[0]);
		System.out.println(out[1]);
		System.out.println(out[2]);
		System.out.println(out[3]);
		System.out.println(out[4]);
		System.out.println(out[5]);
		System.out.println(out[6]);
		System.out.println(out[7]);
	}
}
