package hackerrank.implementation;

public class DivisibleSumPairs4 {
	static int divisibleSumPairs(int n, int k, int[] a) {
		int r = 0;
		for (int i = 0; i < a.length; i++) {
			for (int j = i+1; j < a.length; j++) {
				if ((a[i]+a[j])%k == 0) {
					r++;
				}
			}
		}
		return r;
	}
	
	public static void main(String[] args) {
		int pa = 5;
		int ke = 3;
		int[] is = {2,8,6,8,4};
		int hs = divisibleSumPairs(pa, ke, is);
		System.out.println(hs);
	}
}
