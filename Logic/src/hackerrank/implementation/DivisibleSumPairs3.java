package hackerrank.implementation;

public class DivisibleSumPairs3 {

	static int divisibleSumPairs(int n, int k, int[] array) {
		int hsl = 0;
		for (int i = 0; i < array.length; i++) {
			for (int j = i+1; j < array.length; j++) {
				if ((array[i]+array[j])%k == 0) {
					hsl++;
				}
			}
		}
		return hsl;
	}
	
	public static void main(String[] args) {
		int pjg = 5;
		int kel = 2;
		int[] arr = {5,9,10,7,4};
		int result = divisibleSumPairs(pjg, kel, arr);
		System.out.println(result);
	}
}
