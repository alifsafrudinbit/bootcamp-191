package hackerrank.implementation;

public class BreakingTheRecords3 {
	static int[] breakingTheRecords(int[] scores) {
		int[] result = new int[2];
		int maxS = scores[0];
		int minS = scores[0];
		int maxP = 0;
		int minP = 0;
		for (int i = 0; i < scores.length; i++) {
			if (scores[i]<minS) {
				minP++;
				minS = scores[i];
			}
			if (scores[i]>maxS) {
				maxP++;
				maxS = scores[i];
			}
		}
		result[0] = maxP;
		result[1] = minP;
		return result;
	}
	
	public static void main(String[] args) {
		int[] n = {503,503,503,503,503,503,503,503,503,503,503,503,503,503,503,503};
		int[] result = breakingTheRecords(n);
		System.out.print(result[0]+"\t");
		System.out.println(result[1]);
	}
}
